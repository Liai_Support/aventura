package com.lia.aventurawebview;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;

public class Session {

    private SharedPreferences pref;
    private static final String PREF_NAME = "AventoraPref";

    SharedPreferences.Editor editor;
    // Editor for Shared preferences
    // Context
    Context context;
    // Shared pref mode
    int PRIVATE_MODE = 0;

    private static final String IS_NETWORK = "IsNetwork";
    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "userId";

    public Session(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();    }


    public void setIsNetwork(boolean isNetwork){
        editor.putBoolean(IS_NETWORK, isNetwork);
        editor.commit();
    }

    public boolean isNetwork(){
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return pref.getBoolean(IS_NETWORK,connected);
        }  catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return pref.getBoolean(IS_NETWORK,connected);
    }



    public void nonetwork(){
        AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(context);
       // LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.no_network, null);
        dialoglayout.setMinimumHeight(100);
        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);
        alertDialog3.setView(R.layout.no_network);
        final AlertDialog alertDialog = alertDialog3.create();
        alertDialog.show();
        alertDialog.setCancelable(true);
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
               /* if (isNetwork() == true){
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.show();
                }*/
            }
        });
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = 500;
        layoutParams.height = 500;
        alertDialog.getWindow().setBackgroundDrawable(null);
        alertDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (isNetwork() == true){
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.show();
                }
            }
        });
    }


    public void setmenu(String menu) {
        pref.edit().putString("menu", menu).commit();
    }

    public String getmenu() {
        String menu = pref.getString("menu","home");
        return menu;
    }
}

