package com.lia.aventurawebview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    WebView mywebview;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private Session session;
    public ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen*/
    /*    webView = new WebView(this);
        setContentView(webView);*/
        setContentView(R.layout.activity_main);
        mywebview = (WebView) findViewById(R.id.webView);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("");
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        session = new Session(this);
        if(isNetwork()== false){
            nonetwork();
        }

        /*toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
       // toolbar.setNavigationIcon(R.drawable.ic_mask_group_1);
        //toolbar.setLogo(R.drawable.ic_mask_group_1);
        //toolbar.setTitle("Aventora");
        toolbar.setSubtitle("");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.setHomeAsUpIndicator(R.drawable.ic_mask_group_1);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);*/

        if(StaticInfo.first == true){
           // StaticInfo.first = true;
            mywebview = (WebView) findViewById(R.id.webView);
            // Enable Javascript
            mywebview.getSettings().setJavaScriptEnabled(true);
            // Add a WebViewClient
            mywebview.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {

                    // Inject CSS when page is done loading
                    //injectCSS();
                    progressDialog.dismiss();
                    super.onPageFinished(view, url);
                }
            });
            // Load a webpage
            mywebview.loadUrl("https://yellostack.com/aventura/");
        }
        else {
            if (session.getmenu().equals("home")) {
                navigationView.setCheckedItem(R.id.nav_home);
                mywebview = (WebView) findViewById(R.id.webView);
                // Enable Javascript
                mywebview.getSettings().setJavaScriptEnabled(true);
                // Add a WebViewClient
                mywebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        // Inject CSS when page is done loading
                        //injectCSS();
                        progressDialog.dismiss();
                        super.onPageFinished(view, url);
                    }
                });
                // Load a webpage
                mywebview.loadUrl("https://yellostack.com/aventura/");
            }
            else if (session.getmenu().equals("trips")) {
                navigationView.setCheckedItem(R.id.nav_trips);
                mywebview = (WebView) findViewById(R.id.webView);
                // Enable Javascript
                mywebview.getSettings().setJavaScriptEnabled(true);
                // Add a WebViewClient
                mywebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        // Inject CSS when page is done loading
                        injectCSS();
                        super.onPageFinished(view, url);
                    }
                });
                // Load a webpage
                mywebview.loadUrl("https://yellostack.com/aventura/trips");

            }
            else if (session.getmenu().equals("activities")) {
                navigationView.setCheckedItem(R.id.nav_activities);
                mywebview = (WebView) findViewById(R.id.webView);
                // Enable Javascript
                mywebview.getSettings().setJavaScriptEnabled(true);
                // Add a WebViewClient
                mywebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        // Inject CSS when page is done loading
                        injectCSS();
                        super.onPageFinished(view, url);
                    }
                });
                // Load a webpage
                mywebview.loadUrl("https://yellostack.com/aventura/activities");
            }
            else if (session.getmenu().equals("store")) {
                navigationView.setCheckedItem(R.id.nav_store);
                mywebview = (WebView) findViewById(R.id.webView);
                // Enable Javascript
                mywebview.getSettings().setJavaScriptEnabled(true);
                // Add a WebViewClient
                mywebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        // Inject CSS when page is done loading
                        injectCSS();
                        super.onPageFinished(view, url);
                    }
                });
                // Load a webpage
                mywebview.loadUrl("https://yellostack.com/aventura/store");

            }
            else if (session.getmenu().equals("about")) {
                navigationView.setCheckedItem(R.id.nav_about);
                mywebview = (WebView) findViewById(R.id.webView);
                // Enable Javascript
                mywebview.getSettings().setJavaScriptEnabled(true);
                // Add a WebViewClient
                mywebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        // Inject CSS when page is done loading
                        injectCSS();
                        super.onPageFinished(view, url);
                    }
                });
                // Load a webpage
                mywebview.loadUrl("https://yellostack.com/aventura/aboutus");

            }
            else if (session.getmenu().equals("contact")) {
                navigationView.setCheckedItem(R.id.nav_contact);
                mywebview = (WebView) findViewById(R.id.webView);
                // Enable Javascript
                mywebview.getSettings().setJavaScriptEnabled(true);
                // Add a WebViewClient
                mywebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        // Inject CSS when page is done loading
                        injectCSS();
                        super.onPageFinished(view, url);
                    }
                });
                // Load a webpage
                mywebview.loadUrl("https://yellostack.com/aventura/contactus");
            }
            else if (session.getmenu().equals("login")) {
                navigationView.setCheckedItem(R.id.nav_login);
                mywebview = (WebView) findViewById(R.id.webView);
                // Enable Javascript
                mywebview.getSettings().setJavaScriptEnabled(true);
                // Add a WebViewClient
                /*mywebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        // Inject CSS when page is done loading
                        injectCSS();
                        super.onPageFinished(view, url);
                    }
                });*/
                // Load a webpage
                mywebview.loadUrl("https://yellostack.com/aventura/customer/sign_in");
                progressDialog.dismiss();
            }
            else if (session.getmenu().equals("changelang")) {
                navigationView.setCheckedItem(R.id.nav_changelan);
                mywebview = (WebView) findViewById(R.id.webView);
                // Enable Javascript
                mywebview.getSettings().setJavaScriptEnabled(true);
                // Add a WebViewClient
                mywebview.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {

                        // Inject CSS when page is done loading
                        injectCSS();
                        super.onPageFinished(view, url);
                    }
                });
                // Load a webpage
                mywebview.loadUrl("https://yellostack.com/aventura/customer/changelanguage");
            }
        }

    }



    private void injectCSS() {
        try {
            AssetManager assetManager = getAssets();
            InputStream inputStream = getAssets().open("style.css");
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();
            String encoded = Base64.encodeToString(buffer, Base64.NO_WRAP);
            mywebview.loadUrl("javascript:(function() {" +
                    "var parent = document.getElementsByTagName('head').item(0);" +
                    "var style = document.createElement('style');" +
                    "style.type = 'text/css';" +
                    // Tell the browser to BASE64-decode the string into your script !!!
                    "style.innerHTML = window.atob('" + encoded + "');" +
                    "parent.appendChild(style)" +
                    "})()");
            progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:
                session.setmenu("home");
                Intent intent3 = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent3);
                break;
            case R.id.nav_trips:
                session.setmenu("trips");
                Intent intent1 = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent1);
                break;
            case R.id.nav_activities:
                session.setmenu("activities");
                Intent intent2 = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent2);
                break;
            case R.id.nav_store:
                session.setmenu("store");
                Intent n5 = new Intent(MainActivity.this, MainActivity.class);
                startActivity(n5);
                break;
            case R.id.nav_about:
                session.setmenu("about");
                Intent n6 = new Intent(MainActivity.this, MainActivity.class);
                startActivity(n6);
                break;
            case R.id.nav_contact:
                session.setmenu("contact");
                Intent n7 = new Intent(MainActivity.this, MainActivity.class);
                startActivity(n7);
                break;
            case R.id.nav_login:
                session.setmenu("login");
                Intent n8 = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(n8);
                break;
            case R.id.nav_changelan:
                /*session.setmenu("changelang");
                Intent n9 = new Intent(MainActivity.this, MainActivity.class);
                startActivity(n9);*/
                shouldOverrideUrlLoading(mywebview,"https://yellostack.com/aventura/customer/sign_in");
                break;
            default:
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    public boolean isNetwork(){
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return connected;
        }  catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return connected;
    }

    public void nonetwork(){
        AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(this);
        // LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.no_network, null);
        dialoglayout.setMinimumHeight(100);
        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);
        alertDialog3.setView(R.layout.no_network);
        final AlertDialog alertDialog = alertDialog3.create();
        alertDialog.show();
        alertDialog.setCancelable(true);
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
                if (isNetwork() == true){
                    alertDialog.dismiss();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                else {
                    alertDialog.show();
                }
            }
        });
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = 500;
        layoutParams.height = 500;
        alertDialog.getWindow().setBackgroundDrawable(null);
        alertDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (isNetwork() == true){
                    alertDialog.dismiss();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                else {
                    alertDialog.show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            MainActivity.this.finish();
            System.exit(0);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mywebview.canGoBack()) {
                        mywebview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }


    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}